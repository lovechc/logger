package logger

import (
	"encoding/json"
	"errors"
	"fmt"
	sls "github.com/aliyun/aliyun-log-go-sdk"
	"github.com/gogo/protobuf/proto"
	"io/ioutil"
	"net"
	"os"
	"time"
)

const (
	LevelEmergency     = "EMER"    // 系统级紧急，比如磁盘出错，内存异常，网络不可用等
	LevelAlert         = "ALRT"    // 系统级警告，比如数据库访问异常，配置文件出错等
	LevelCritical      = "CRIT"    // 系统级危险，比如权限出错，访问异常等
	LevelError         = "Error"   // 用户级错误
	LevelWarning       = "Warning" // 用户级警告
	LevelInformational = "Info"    // 用户级信息
	LevelDebug         = "Debug"   // 用户级调试
	LevelTrace         = "Trace"   // 用户级基本输出
)

// 默认日志输出
var defaultLogger *LocalLogger

//配置文件信息
type logConfig struct {
	Endpoint        string `json:"endpoint"`          //日志服务的服务入口
	AccessKeyId     string `json:"access_key_id"`     //阿里云api access_key_id
	AccessKeySecret string `json:"access_key_secret"` //阿里云api access_key_secret
	SecurityToken   string `json:"security_token"`    //RAM用户角色的临时安全令牌。此处取值为空，表示不使用临时安全令牌
	ProjectName     string `json:"project_name"`      //项目实例名称 - 可通过控制台或api创建
	LogStoreName    string `json:"log_store_name"`    //日志存储实例名称 - 可通过控制台或api创建
}

//业务错误：其实就是一些操作上的错误，比如登陆时用户名或者密码错误，新建用户时账号已被注册等。这一类的错误应该尽可能地给用户给出准确的提示信息，后台日志不应该打印出错误堆栈信息。
//系统异常：包括系统挂了，服务间调用失败，数据库异常，内存溢出等等，这类错误信息可以给用户提示“系统异常，请联系管理员”等类似的提示，不需要具体提示，因为用户不会关心究竟是服务异常还是数据库异常。但此时后台日志应该记录详细的错误堆栈信息，以便于运维或者开发人员定位问题。

//日志信息
type LogInfo struct {
	Content    string //用户自定义日志内容
	Type       string //日志类型 System：系统错误 Business:业务错误
	Line       string //错误行号信息
	StackTrace string //错误栈内容
	HttpReq    string //http请求信息
	UserId     string //用户id
}

type LocalLogger struct {
	client       sls.ClientInterface //日志服务客户端
	projectName  string              //项目名
	logStoreName string              //项目日志名
}

func (t *LocalLogger) writeMsg(logLevel string, v ...LogInfo) error {
	var logs []*sls.Log
	for _, item := range v {
		if item.Type == "" { //如果没有值，默认业务错误
			item.Type = "Business"
		}
		itemJson, err := json.Marshal(item)
		if err != nil {
			return err
		}
		var logM map[string]string
		err = json.Unmarshal(itemJson, &logM)
		if err != nil {
			return err
		}

		var content []*sls.LogContent
		for k, v := range logM {
			content = append(content, &sls.LogContent{Key: proto.String(k), Value: proto.String(v)})
		}

		logItem := &sls.Log{
			Time:     proto.Uint32(uint32(time.Now().Unix())),
			Contents: content,
		}

		logs = append(logs, logItem)
	}

	ip, _ := GetIp()
	logGroup := &sls.LogGroup{
		Topic:  proto.String(logLevel), //主题
		Source: proto.String(ip),       //来源
		Logs:   logs,                   //日志
	}

	err := t.client.PutLogs(t.projectName, t.logStoreName, logGroup)
	return err
}

func Info(v ...LogInfo) {
	_ = defaultLogger.writeMsg(LevelInformational, v...)
}

func Warn(v ...LogInfo) {
	_ = defaultLogger.writeMsg(LevelWarning, v...)
}

func Error(v ...LogInfo) {
	_ = defaultLogger.writeMsg(LevelError, v...)
}

//设置日志信息
//@param param string json配置文件路径 或者 json 字符串
func SetLogger(param string) error {
	conf := new(logConfig)
	err := json.Unmarshal([]byte(param), conf)

	//不是json字符串，就认为是配置文件，如果都不是，打印日志，然后退出
	if err != nil {
		fd, err := os.Open(param)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not open %s for configure: %s\n", param, err)
			os.Exit(1)
			return err
		}
		contents, err := ioutil.ReadAll(fd)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not read %s: %s\n", param, err)
			os.Exit(1)
			return err
		}
		err = json.Unmarshal(contents, conf)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not Unmarshal %s: %s\n", contents, err)
			os.Exit(1)
			return err
		}
	}

	err = func() error {
		client := sls.CreateNormalInterface(conf.Endpoint, conf.AccessKeyId, conf.AccessKeySecret, conf.SecurityToken)
		if client == nil {
			return errors.New("创建日志客户端Error:客户创建失败")
		}
		defaultLogger = new(LocalLogger)
		defaultLogger.client = client
		defaultLogger.projectName = conf.ProjectName
		defaultLogger.logStoreName = conf.LogStoreName
		return nil
	}()
	if err != nil {
		fmt.Fprintf(os.Stderr, "连接日志服务Error: %s\n", err)
		os.Exit(1)
		return err
	}
	return nil
}

//根据本机ip
func GetIp() (ip string, err error) {
	netInterfaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for i := 0; i < len(netInterfaces); i++ {
		if (netInterfaces[i].Flags & net.FlagUp) != 0 {
			addRs, _ := netInterfaces[i].Addrs()
			for _, address := range addRs {
				if ipNet, ok := address.(*net.IPNet); ok && !ipNet.IP.IsLoopback() {
					return ipNet.IP.String(), nil
				}
			}
		}
	}
	return "", nil
}
