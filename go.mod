module gitee.com/lovechc/logger

go 1.15

require (
	github.com/aliyun/aliyun-log-go-sdk v0.1.23
	github.com/gogo/protobuf v1.3.2
)
