package logger_test

import (
	"gitee.com/lovechc/logger"
	"testing"
)

func TestWLog(t *testing.T) {
	//设置日志信息
	err := logger.SetLogger("config.json")
	if err != nil {
		panic(err)
	}
	logger.Info(logger.LogInfo{Content: "trace"})
	logger.Warn(logger.LogInfo{Content: "warn", Line: "xxx文件 18 line", StackTrace: "错误栈内容", HttpReq: "http 请求string", UserId: "10"})
	logger.Error(logger.LogInfo{Content: "error", Line: "xxx文件 18 line", StackTrace: "错误栈内容", HttpReq: "http 请求string", UserId: "10"})
}
